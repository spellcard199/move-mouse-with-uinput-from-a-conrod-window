extern crate conrod_core;
extern crate conrod_glium;
extern crate conrod_winit;
extern crate find_folder;
extern crate glium;
extern crate image;
extern crate uinput;

use std::thread;
use std::time::Duration;
use uinput::event::controller::Controller::Mouse;
use uinput::event::controller::Mouse::{Left, Right};
use uinput::event::Event::{Controller, Relative};
use uinput::event::relative::Position::{X, Y};
use uinput::event::relative::Relative::Position;
use glium::Surface;

mod support;

struct MouseOffset {
    x: i32,
    y: i32,
}

fn mk_device() -> uinput::Device {
    let device = uinput::open("/dev/uinput").unwrap()
        .name("test").unwrap()
        // It's necessary to enable any mouse button. Otherwise Relative events would not work.
        // .event(uinput::event::Keyboard::All).unwrap()    // used while learning
        .event(Controller(Mouse(Left))).unwrap()
        .event(Controller(Mouse(Right))).unwrap()
        .event(Relative(Position(X))).unwrap()
        .event(Relative(Position(Y))).unwrap()
        .create().unwrap();
    // It needs time to make a device. If you don't wait enough you lose the events sent.
    thread::sleep(Duration::from_millis(100));
    return device;
}

fn mouse_move(mut mouse_device: uinput::Device,
              mouse_offset: MouseOffset)
              -> uinput::Device {
    mouse_device.send(X, mouse_offset.x).unwrap();
    mouse_device.send(Y, mouse_offset.y).unwrap();
    mouse_device.synchronize().unwrap();
    return mouse_device;
}

fn mouse_click(mut mouse_device: uinput::Device,
               button: uinput::event::Controller)
               -> uinput::Device {
    mouse_device.press(&button).unwrap();
    mouse_device.synchronize().unwrap();
    // It seems to me that if you don't wait enough /dev/uinput gets "corrupted"
    // - In one case my linux crashed
    // - In another case I couldn't click anymore and cursor disappeared in some areas of the screen
    thread::sleep(Duration::from_millis(100));
    return mouse_device;
}

fn match_window_event(mouse_device: uinput::Device,
                      win_event: glium::glutin::WindowEvent,
                      speed: i32)
                      -> uinput::Device {
    match win_event {
        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Up),
                ..
            },
            ..
        } => return mouse_move(mouse_device,
                               MouseOffset { x: 0, y: -speed }),

        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Down),
                ..
            },
            ..
        } => return mouse_move(mouse_device,
                               MouseOffset { x: 0, y: speed }),

        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Left),
                ..
            },
            ..
        } => return mouse_move(mouse_device,
                               MouseOffset { x: -speed, y: 0 }),

        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Right),
                ..
            },
            ..
        } => return mouse_move(mouse_device,
                               MouseOffset { x: speed, y: 0 }),

        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Return),
                ..
            },
            ..
        } => {
            mouse_click(mouse_device, Mouse(Left));
            std::process::exit(0);
        },

        glium::glutin::WindowEvent::KeyboardInput {
            input: glium::glutin::KeyboardInput {
                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Back),
                ..
            },
            ..
        } => {
            mouse_click(mouse_device, Mouse(Right));
            std::process::exit(0);
        },

        _ => return mouse_device
    }
}

fn main() {
    // TODO: if another instance is running, just activate that (maybe with `xdotool windowactivate ...`?)
    const WIDTH: u32 = 8;
    const HEIGHT: u32 = 6;

    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Canvas")
        .with_dimensions((WIDTH, HEIGHT).into());
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).unwrap();
    let display = support::GliumDisplayWinitWrapper(display);

    // construct our `Ui`.
    let mut ui = conrod_core::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    // A type used for converting `conrod_core::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod_glium::Renderer::new(&display.0).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod_core::image::Map::<glium::texture::Texture2d>::new();

    let mut mouse_device = mk_device();
    let mut mouse_speed = 50;

    // Poll events from the window.
    let mut event_loop = support::EventLoop::new();
    'main: loop {

        // Handle all events.
        for event in event_loop.next(&mut events_loop) {

            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(event) = support::convert_event(event.clone(), &display) {
                ui.handle_event(event);
                event_loop.needs_update();
            }

            match event {
                glium::glutin::Event::WindowEvent { event, .. } => match event {
                    // Break from the loop upon `Escape`.
                    glium::glutin::WindowEvent::CloseRequested |
                    glium::glutin::WindowEvent::KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                            ..
                        },
                        ..
                    } => break 'main,

                    glium::glutin::WindowEvent::KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(glium::glutin::VirtualKeyCode::PageUp),
                            ..
                        },
                        ..
                    } => mouse_speed = 50,

                    glium::glutin::WindowEvent::KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(glium::glutin::VirtualKeyCode::PageDown),
                            ..
                        },
                        ..
                    } => mouse_speed = 10,

                    _ => mouse_device = match_window_event(mouse_device,
                                                           event,
                                                           mouse_speed),
                },
                _ => (),
            }
        }

        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = ui.draw_if_changed() {
            renderer.fill(&display.0, primitives, &image_map);
            let mut target = display.0.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            renderer.draw(&display.0, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }
}
